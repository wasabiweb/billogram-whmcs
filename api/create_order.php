<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Billogram\Api as BillogramAPI;
use Billogram\Api\Exceptions\ObjectNotFoundError;

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) .
            DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require $fileName;
}
spl_autoload_register('autoload');

// Include Config
$config = include 'config.php';

// Load Billogram API in $api
$api = new BillogramAPI(
  $config['billogram']['api_id'],
  $config['billogram']['api_password'],
  $config['billogram']['identifier'],
  $config['billogram']['url']
);

// Include Function
include('functions.php');


/*----------------------------------------------------------------------------*/

redirect_home_if_order_is_done();

// Get additional info about the client from WHMCS based on email address
$params['email'] = urldecode($_POST['email']);
$extra_client_info_from_crm = get_client_details_from_whmcs($config, $params);

// Check if the customer exists in Billogram, based on the company name from the WHMCS
$params['companyname'] = $extra_client_info_from_crm['companyname'];
$customer_exists_in_billogram = check_if_client_exists_in_billogram($api, $params);

if ($customer_exists_in_billogram == false) {

    // Create client
    $postcode = str_replace(' ', '', $extra_client_info_from_crm["postcode"]);
    $params = array(
        'companyname' => $extra_client_info_from_crm["companyname"],
        'address' => $extra_client_info_from_crm["address1"],
        'postcode' => $postcode,
        'city' => $extra_client_info_from_crm["city"],
        'email' => $extra_client_info_from_crm["customfields6"]
    );
    $billogram_client = create_billogram_client($api, $params);

    // Check if invoice exists, if it does redirect home to WHMCS
    check_for_duplicate_billogram_invoice($config, $params);

    // If the invoice doesnt exist, Create the Invoice
    $params['customer_no'] = $billogram_client->customer_no;
    create_billogram_invoice($config, $api, $params);

}

if ($customer_exists_in_billogram == true) {

    // Check if invoice exists, if invoice exists redirect to WHMCS
    check_for_duplicate_billogram_invoice($config, $params);

    // If the invoice doesnt exist, Create the Invoice
    $params['customer_no'] = $customer_exists_in_billogram->customer_no;
    create_billogram_invoice($config, $api, $params);

}
