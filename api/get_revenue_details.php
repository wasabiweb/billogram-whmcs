<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Billogram\Api as BillogramAPI;
use Billogram\Api\Exceptions\ObjectNotFoundError;

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) .
            DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require $fileName;
}
spl_autoload_register('autoload');

// Include Config
$config = include 'config.php';

// Load Billogram API in $api
$api = new BillogramAPI(
  $config['billogram']['api_id'],
  $config['billogram']['api_password'],
  $config['billogram']['identifier'],
  $config['billogram']['url']
);

// Include Function
include('functions.php');


/*----------------------------------------------------------------------------*/

?>

<!-- <style>
table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

table td, table th {
    border: 1px solid #ddd;
    padding: 8px;
}

table tr:nth-child(even){background-color: #f2f2f2;}

table tr:hover {background-color: #ddd;}

table th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #57bbb1;
    color: white;
}

.total {
  font-weight: bold;
}
</style> -->

<!-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	function tally (selector) {
		$(selector).each(function () {
			var total = 0,
				column = $(this).siblings(selector).andSelf().index(this);
			$(this).parents().prevUntil(':has(' + selector + ')').each(function () {
				total += parseFloat($('td.sum:eq(' + column + ')', this).html()) || 0;
			})
			$(this).html(total);
		});
	}
	// tally('td.subtotal');
	tally('td.total');
});

</script> -->

<?php

$account = $_GET['account'];
$month = $_GET['month'];

$details = get_revenue_details($account, $month);

foreach ($details as $row) {
  echo $row['id'];
  echo ' - ';
  echo $row['customer'];
  echo ' - ';
  echo $row['amount'];
  echo 'kr';
  echo '<br><br>';
}

?>
