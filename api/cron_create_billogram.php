<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Billogram\Api as BillogramAPI;
use Billogram\Api\Exceptions\ObjectNotFoundError;

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) .
            DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require $fileName;
}
spl_autoload_register('autoload');

// Include Config
$config = include 'config.php';

// Load Billogram API in $api
$api = new BillogramAPI(
  $config['billogram']['api_id'],
  $config['billogram']['api_password'],
  $config['billogram']['identifier'],
  $config['billogram']['url']
);

// Include Function
include('functions.php');


/*----------------------------------------------------------------------------*/

$whmcs_invoices = get_all_whmcs_invoices($config);

foreach ($whmcs_invoices['invoices']['invoice'] as $invoice) {

  $invoice_date = explode('-', $invoice['date']);
  $invoice_year = $invoice_date['0'];
  $current_year = date('Y');

  // If this invoice has no note, it hasent been sent to billogram
  if ($invoice_year == $current_year && $invoice['id'] >= 4768) {
    if ($invoice['notes']) {
      // Invoice has been sent, do nothing
    } else {
      var_dump($invoice);

      // Get additional info about the client from WHMCS based on email address
      $params['client_id'] = $invoice['userid'];
      $extra_client_info_from_crm = get_client_details_from_whmcs($config, $params);

      // Check if the customer exists in Billogram, based on the company name from the WHMCS
      $params['companyname'] = $extra_client_info_from_crm['companyname'];
      $customer_exists_in_billogram = check_if_client_exists_in_billogram($api, $params);

      $params['invoice_id'] = $invoice['id'];

      if ($customer_exists_in_billogram == false) {

          // Create client
          $postcode = str_replace(' ', '', $extra_client_info_from_crm["postcode"]);
          $params = array(
              'companyname' => $extra_client_info_from_crm["companyname"],
              'address' => $extra_client_info_from_crm["address1"],
              'postcode' => $postcode,
              'city' => $extra_client_info_from_crm["city"],
              'email' => $extra_client_info_from_crm["customfields6"]
          );
          $billogram_client = create_billogram_client($api, $params);

          // Create the Invoice
          $params['customer_no'] = $billogram_client->customer_no;
          create_billogram_invoice($config, $api, $params);
      }

      if ($customer_exists_in_billogram == true) {

          // Create the Invoice
          $params['customer_no'] = $customer_exists_in_billogram->customer_no;
          create_billogram_invoice($config, $api, $params);
      }
    }
  }
}
