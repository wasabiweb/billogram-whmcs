<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Billogram\Api as BillogramAPI;
use Billogram\Api\Exceptions\ObjectNotFoundError;

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) .
            DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require $fileName;
}
spl_autoload_register('autoload');

// Include Config
$config = include 'config.php';

// Load Billogram API in $api
$api = new BillogramAPI(
  $config['billogram']['api_id'],
  $config['billogram']['api_password'],
  $config['billogram']['identifier'],
  $config['billogram']['url']
);

// Include Function
include('functions.php');


/*----------------------------------------------------------------------------*/

echo '<pre>';
$save_all_billograms = save_all_billograms_to_json($api);

$save_detailed_billograms_to_json = save_detailed_billograms_to_json($api);

// var_dump($save_all_billograms);
var_dump($save_detailed_billograms_to_json);

echo '</pre>';
