<?php

/**
 * REST client for Grandid
 * Return JSON
 * v 2.0.3
 */

/**
 * @var string  api Key
 */
$apiKey = "3c103a0db2a5f464c38e1bfffc157a31";

/**
 * @var  string authenticate Service Key
 */
$authenticateServiceKey = "d029f49ede810172f52217acd84c675c";

/**
 * @var  string redirect Url
 */
$redirect_url = "http://billogram.wasabiweb.se/login_to_whmcs.php";

/**
 * @var string Query Url
 */
$query_url = "https://client-test.grandid.com/json1.1/FederatedLogin?apiKey=$apiKey&authenticateServiceKey=$authenticateServiceKey&callbackUrl=$redirect_url";

/**
 * @var string WHMCS URL
 */
$whmcsUrl = "https://admin.wasabiweb.se/";

/**
 * @var string WHMCS UserName
 */
$username = "andreas@wasabiweb.se";

/**
 * @var string WHMCS Password
 */
$password = md5("MT1AXKmK0SHexwp");

/**
 * @var string WHMCS AutoAuth URL
 */
$whmcsurl = "https://admin.wasabiweb.se/dologin.php";

/**
 * @var string WHMCS AutoAuth Password Key
 */
$autoauthkey = "7dGoIKN9X0Ls";

// Start Query
$query = file_get_contents($query_url);
$query = json_decode($query);

// Check if login was successful
if (isset($_GET['grandidsession'])) {

  // Logged in, make sure to get name and then AuthAuth the user into WHMCS
  $session_id = $_GET['grandidsession'];
  $session_url = "https://client-test.grandid.com/json1.1/GetSession?sessionid=$session_id&apiKey=$apiKey&authenticateServiceKey=$authenticateServiceKey";
  $session_query = file_get_contents($session_url);
  $session_query = json_decode($session_query);
  $firstName = $session_query->userAttributes->givenName;
  $lastName = $session_query->userAttributes->surname;
  $fullName = $session_query->userAttributes->name;

  // Get the Full Name of the person who logged in, and log in the the same account in WHMCS
  $postfields = array(
    'username' => $username,
    'password' => $password,
    'action' => 'GetClients',
    'search' => $fullName,
    'responsetype' => 'json'
  );

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'includes/api.php');
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
  $response = curl_exec($ch);
  if (curl_error($ch)) {
  	die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
  }
  curl_close($ch);

  $clientData = json_decode($response, true);



  if ($clientData['totalresults'] > 0) {

      $email_to_login = $clientData['clients']['client']['0']['email'];

  } else {

    // Get the Full Name of the person who logged in, and log in the the same account in WHMCS
    $postfields = array(
    	'username' => $username,
    	'password' => $password,
    	'action' => 'GetContacts',
      'firstname' => $firstName,
      'lastname' => $lastName,
    	'responsetype' => 'json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'includes/api.php');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
    $response = curl_exec($ch);
    if (curl_error($ch)) {
    	die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);

    $contactData = json_decode($response, true);

    $email_to_login = $contactData['contacts']['contact']['0']['email'];

  }


  if ($email_to_login) {

    $timestamp = time(); # Get current timestamp
    $email = $email_to_login; # Clients Email Address to Login
    $goto = "clientarea.php";

    $hash = sha1($email.$timestamp.$autoauthkey); # Generate Hash

    # Generate AutoAuth URL & Redirect
    $url = $whmcsurl."?email=$email&timestamp=$timestamp&hash=$hash&goto=".urlencode($goto);
    header("Location: $url");
    exit;

  } else {

    echo 'Något blev fel i din inloggning, testa att logga in med e-post/lösenord istället eller kontakta oss på support@wasabiweb.se';

  }

} else {

  // If login hasent been made, send the user to the login screen
  if (isset($query->sessionId)) {

    header("Location: $query->redirectUrl"); exit;

  }
}
