<?php

$access_token = "683d4fa30ba1391d8a0395b5ab269522359645faf756284e80de9bf89a3e0316";
$client_url = $_GET['client_url'];
$keywords = array();

$curl_url = curl_init();

curl_setopt_array($curl_url, array(
  CURLOPT_URL => "http://api.ranktrackr.com/api/v1/urls?access_token=$access_token&search=$client_url",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/json",
    "cache-control: no-cache"
  ),
));

$response_url = curl_exec($curl_url);
$err_url = curl_error($curl_url);

curl_close($curl_url);

if ($err_url) {
  echo "cURL Error #:" . $err_url;
} else {
  $response_url = json_decode($response_url);

  $url_id = $response_url[0]->id;

  $curl_keywords = curl_init();

  curl_setopt_array($curl_keywords, array(
    CURLOPT_URL => "http://api.ranktrackr.com/api/v1/urls/$url_id/keywords/results?access_token=$access_token",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_POSTFIELDS => "",
    CURLOPT_HTTPHEADER => array(
      "Content-Type: application/json",
      "cache-control: no-cache"
    ),
  ));

  $response_keywords = curl_exec($curl_keywords);
  $err_keywords = curl_error($curl_keywords);

  curl_close($curl_keywords);

  if ($err_keywords) {
    echo "cURL Error #:" . $err_keywords;
  } else {
    $response_keywords = json_decode($response_keywords);
    $i = 0;

    foreach ($response_keywords as $keyword) {
      $i++;

      $keywords[$i]['keyword'] = $keyword->query;
      $keywords[$i]['position'] = $keyword->results[0]->position_organic;
      $position_changed = $keyword->results[0]->position_change_cache[0];

      if ($position_changed == NULL) {
        $position_changed = '0';
      }

      $keywords[$i]['position_changed'] = $position_changed;
    }
  }
}

$keywords = json_encode($keywords);
echo $keywords;
