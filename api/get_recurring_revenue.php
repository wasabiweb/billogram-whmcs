<?php

$whmcsUrl = "https://admin.wasabiweb.se/";

$username = "andreas@wasabiweb.se";
$password = md5("MT1AXKmK0SHexwp");

$postfields = array(
	'username' => $username,
	'password' => $password,
	'action' => 'GetClientsProducts',
	'responsetype' => 'json',
	'limitnum' => 0
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $whmcsUrl . 'includes/api.php');
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
$response = curl_exec($ch);
if (curl_error($ch)) {
	die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
}
curl_close($ch);

$jsonData = json_decode($response, true);

$products = $jsonData['products'];


foreach($products['product'] as $product) {
	if ($product['status'] == 'Active') {


		// Hosting
		if ($product['groupname'] == 'Driftpaket') {

			$hostingRecurringAmount = round($product['recurringamount']);

			switch ($product['billingcycle']) {
				case Annually:
					$hostingAnnually = $hostingAnnually + $hostingRecurringAmount;
					break;

				case "Semi-Annually":
					$hostingAnnually = ($hostingRecurringAmount * 2) + $hostingAnnually;
					break;

				case Quarterly:
					$hostingAnnually = ($hostingRecurringAmount * 3) + $hostingAnnually;
					break;

				case Monthly:
					$hostingAnnually = ($hostingRecurringAmount * 12) + $hostingAnnually;
					break;

				default:
					if ($hostingRecurringAmount != 0) {
						echo "Följande hosting kund faktureras inte korrekt ";
						echo $product['domain']; echo "<br>";
					}
					break;
			}

		}

		// SEO
		if ($product['groupname'] == 'SEO') {

			$seoRecurringAmount = round($product['recurringamount']);

			switch ($product['billingcycle']) {
				case Annually:
					$seoAnnually = $seoAnnually + $seoRecurringAmount;
					break;

				case Quarterly:
					$seoAnnually = ($seoRecurringAmount * 3) + $seoAnnually;
					break;

				case Monthly:
					$seoAnnually = ($seoRecurringAmount * 12) + $seoAnnually;
					break;

				default:
					if ($seoRecurringAmount != 0) {
						echo "Följande SEO kund faktureras inte korrekt ";
						var_dump($product); echo "<br>";
					}
					break;
			}

		}

		// Content Marketing
		if ($product['groupname'] == 'Content Marketing') {

			$cmRecurringAmount = round($product['recurringamount']);

			switch ($product['billingcycle']) {
				case Annually:
					$cmAnnually = $cmAnnually + $cmRecurringAmount;
					break;

				case Quarterly:
					$cmAnnually = ($cmRecurringAmount * 3) + $cmAnnually;
					break;

				case Monthly:
					$cmAnnually = ($cmRecurringAmount * 12) + $cmAnnually;
					break;

				default:
					if ($cmRecurringAmount != 0) {
						echo "Följande Content Marketing kund faktureras inte korrekt ";
						var_dump($product); echo "<br>";
					}
					break;
			}

		}

		// Video
		if ($product['groupname'] == 'Video') {

			$videoRecurringAmount = round($product['recurringamount']);

			switch ($product['billingcycle']) {
				case Annually:
					$videoAnnually = $videoAnnually + $videoRecurringAmount;
					break;

				case Quarterly:
					$videoAnnually = ($videoRecurringAmount * 3) + $videoAnnually;
					break;

				case Monthly:
					$videoAnnually = ($videoRecurringAmount * 12) + $videoAnnually;
					break;

				default:
					if ($videoRecurringAmount != 0) {
						echo "Följande Video kund faktureras inte korrekt ";
						var_dump($product); echo "<br>";
					}
					break;
			}

		}

		// Sociala Medier
		if ($product['groupname'] == 'Sociala Medier') {

			$smRecurringAmount = round($product['recurringamount']);

			switch ($product['billingcycle']) {
				case Annually:
					$smAnnually = $smAnnually + $smRecurringAmount;
					break;

				case Quarterly:
					$smAnnually = ($smRecurringAmount * 3) + $smAnnually;
					break;

				case Monthly:
					$smAnnually = ($smRecurringAmount * 12) + $smAnnually;
					break;

				default:
					if ($smRecurringAmount != 0) {
						echo "Följande Sociala Media kund faktureras inte korrekt ";
						var_dump($product); echo "<br>";
					}
					break;
			}

		}

		// Serviceavtal
		if ($product['groupname'] == 'Serviceavtal') {

			$saRecurringAmount = round($product['recurringamount']);

			switch ($product['billingcycle']) {
				case Annually:
					$saAnnually = $saAnnually + $saRecurringAmount;
					break;

				case Quarterly:
					$saAnnually = ($saRecurringAmount * 3) + $saAnnually;
					break;

				case Monthly:
					$saAnnually = ($saRecurringAmount * 12) + $saAnnually;
					break;

				default:
					if ($saRecurringAmount != 0) {
						echo "Följande Serviceavtal kund faktureras inte korrekt ";
						var_dump($product); echo "<br>";
					}
					break;
			}

		}

    // CMS
    if ($product['groupname'] == 'CMS') {

        $cmsRecurringAmount = round($product['recurringamount']);

        switch ($product['billingcycle']) {
            case Annually:
                $cmsAnnually = $cmsAnnually + $cmsRecurringAmount;
                break;

            case Quarterly:
                $cmsAnnually = ($cmsRecurringAmount * 3) + $cmsAnnually;
                break;

            case Monthly:
                $cmsAnnually = ($cmsRecurringAmount * 12) + $cmsAnnually;
                break;

            default:
                if ($cmsRecurringAmount != 0) {
                    echo "Följande CMS kund faktureras inte korrekt ";
                    var_dump($product); echo "<br>";
                }
                break;
        }

    }

		// Adwords
    if ($product['groupname'] == 'Adwords') {

        $adwordsRecurringAmount = round($product['recurringamount']);

        switch ($product['billingcycle']) {
            case Annually:
                $adwordsAnnually = $adwordsAnnually + $adwordsRecurringAmount;
                break;

            case Quarterly:
                $adwordsAnnually = ($adwordsRecurringAmount * 3) + $adwordsAnnually;
                break;

            case Monthly:
                $adwordsAnnually = ($adwordsRecurringAmount * 12) + $adwordsAnnually;
                break;

            default:
                if ($adwordsRecurringAmount != 0) {
                    echo "Följande Adwords kund faktureras inte korrekt ";
                    var_dump($product); echo "<br>";
                }
                break;
        }

    }


	}
}

$hostingMonthly = $hostingAnnually / 12;
$seoMonthly = $seoAnnually / 12;
$cmMonthly = $cmAnnually / 12;
$videoMonthly = $videoAnnually / 12;
$smMonthly = $smAnnually / 12;
$saMonthly = $saAnnually / 12;
$cmsMonthly = $cmsAnnually / 12;
$adwordsMonthly = $adwordsAnnually / 12;
?>

<html>
<head>
	<title>Budget</title>
</head>
<body>
	<!-- 1: Hosting -->
	<table>
		<tr>
			<td>
				<?php echo round($hostingMonthly); ?>
			</td>
		</tr>
	</table>

    <!-- 2: SEO -->
    <table>
        <tr>
            <td>
                <?php echo round($seoMonthly); ?>
            </td>
        </tr>
    </table>

    <!-- 3: Content Marketing -->
    <table>
        <tr>
            <td>
                <?php echo round($cmMonthly); ?>
            </td>
        </tr>
    </table>

    <!-- 4: Video -->
    <table>
        <tr>
            <td>
                <?php echo round($videoMonthly); ?>
            </td>
        </tr>
    </table>

    <!-- 5: Sociala Medier -->
    <table>
        <tr>
            <td>
                <?php echo round($smMonthly); ?>
            </td>
        </tr>
    </table>

    <!-- 6: Serviceavtal -->
    <table>
        <tr>
            <td>
                <?php echo round($saMonthly); ?>
            </td>
        </tr>
    </table>

    <!-- 7: CMS -->
    <table>
        <tr>
            <td>
                <?php echo round($cmsMonthly); ?>
            </td>
        </tr>
    </table>

		<!-- 8: Adwords -->
    <table>
        <tr>
            <td>
                <?php echo round($adwordsMonthly); ?>
            </td>
        </tr>
    </table>

</body>
</html>
