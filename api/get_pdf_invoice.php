<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Billogram\Api as BillogramAPI;
use Billogram\Api\Exceptions\ObjectNotFoundError;

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) .
            DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require $fileName;
}
spl_autoload_register('autoload');

// Include Config
$config = include 'config.php';

// Load Billogram API in $api
$api = new BillogramAPI(
  $config['billogram']['api_id'],
  $config['billogram']['api_password'],
  $config['billogram']['identifier'],
  $config['billogram']['url']
);

// Include Function
include('functions.php');


/*----------------------------------------------------------------------------*/

$invoice_no = $_GET['invoice_no'];
$invoice = get_pdf_invoice($api, $invoice_no);

echo "<iframe src='data:application/pdf;base64,$invoice' style='width:100%; height:100%;' frameborder='0'></iframe>";
