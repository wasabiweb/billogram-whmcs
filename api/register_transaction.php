<?php

/* Include the Billogram API library */
use Billogram\Api as BillogramAPI;
use Billogram\Api\Exceptions\ObjectNotFoundError;

/* Sample autoloader function for PHP. For most applications, you usually
   already have one of these registered.
*/
function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) .
            DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require $fileName;
}
spl_autoload_register('autoload');

// Include config and functions file
$config = include 'config.php';
include_once('functions.php');

// Load Billogram API in $api
$params = array(
    'api_id' => $config['billogram']['api_id'],
    'api_password' => $config['billogram']['api_password'],
    'identifier' => $config['billogram']['identifier'],
    'url' => $config['billogram']['url'],
);
$api = new BillogramAPI($params['api_id'], $params['api_password'], $params['identifier'], $params['url']);

/*------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

$callback_from_billogram = file_get_contents('php://input');

file_put_contents('log.json', var_export(file_get_contents('php://input'), true), FILE_APPEND);

$callback_from_billogram = json_decode($callback_from_billogram, true);

register_payment_in_crm($config, $api, $callback_from_billogram);