<?php

function get_all_whmcs_invoices($config) {

  // Set post values
  $postfields = array(
      'username' => $config['whmcs']['username'],
      'password' => $config['whmcs']['password'],
      'action' => 'GetInvoices',
      'limitnum' => 99999,
      'responsetype' => 'json',
  );

  // Call the API
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $config['whmcs']['url'] . 'includes/api.php');
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
  $response = curl_exec($ch);
  if (curl_error($ch)) {
      die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
  }
  curl_close($ch);

  // Attempt to decode response as json
  return json_decode($response, true);
}

function redirect_home_if_order_is_done () {

    $referrer = $_SERVER['HTTP_REFERER'];
    $referrer = explode('/', $referrer);
    $referrer = explode('?', $referrer['3']);

    if ($referrer == 'cart.php') {
        header("Location: http://admin.wasabiweb.se/tack.php");
    }
}

function get_client_details_from_whmcs ($config, $params) {

    // Set post values
    $postfields = array(
        'username' => $config['whmcs']['username'],
        'password' => $config['whmcs']['password'],
        'action' => 'GetClientsDetails',
        'email' => $params['email'],
        'clientid' => $params['client_id'],
        'responsetype' => 'json',
    );

    // Call the API
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $config['whmcs']['url'] . 'includes/api.php');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
    $response = curl_exec($ch);
    if (curl_error($ch)) {
        die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);

    // Attempt to decode response as json
    return json_decode($response, true);
}

function check_if_client_exists_in_billogram ($api, $params) {

    $customersQuery = $api->customers->query()->order('created_at', 'asc');
    $totalPages = $customersQuery->totalPages();

    for ($page = 1; $page <= $totalPages; $page++) {

        $customersArray = $customersQuery->getPage($page);

        foreach ($customersArray as $customer) {

            if ($customer->name == $params['companyname']) {
                return $customer;
            }
        }
    }

}

function create_billogram_client ($api, $params) {

    $create_customer = $api->customers->create(array(
        'name' => $params['companyname'],
        'address' => array(
            'street_address' => $params['address'],
            'zipcode' => $params['postcode'],
            'city' => $params['city'],
        ),
        'contact' => array(
            'email' => $params['email'],
        ),
    ));

    return $create_customer;
}

function check_for_duplicate_billogram_invoice ($config, $params) {

    if ($_POST['invoice_id'] == false) {
      $invoice_id = $params['invoice_id'];
    } else {
      $invoice_id = $_POST['invoice_id'];
    }

    // Set post values
    $postfields = array(
        'username' => $config['whmcs']['username'],
        'password' => $config['whmcs']['password'],
        'action' => 'getinvoice',
        'invoiceid' => $invoice_id,
        'responsetype' => 'json',
    );

    // Call the API
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $config['whmcs']['url'] . 'includes/api.php');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
    $response = curl_exec($ch);
    if (curl_error($ch)) {
        die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);

    // Attempt to decode response as json
    $invoiceDetails = json_decode($response, true);

    if ($invoiceDetails['notes']) {
        header("Location: http://admin.wasabiweb.se/tack.php"); die();
    }
}

function create_billogram_invoice ($config, $api, $params) {

    // Cron Job sends invoice through this function instead of listening to $_POST what is happening when you send invoice manually
    if ($_POST['invoice_id'] == false) {
      $invoice_id = $params['invoice_id'];
    } else {
      $invoice_id = $_POST['invoice_id'];
    }

    if ($invoice_id) {
      // Set post values
      $postfields = array(
        'username' => $config['whmcs']['username'],
        'password' => $config['whmcs']['password'],
        'action' => 'getinvoice',
        'invoiceid' => $invoice_id,
        'responsetype' => 'json',
      );

      // Call the API
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $config['whmcs']['url'] . 'includes/api.php');
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
      $response = curl_exec($ch);
      if (curl_error($ch)) {
        die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
      }
      curl_close($ch);

      // Attempt to decode response as json
      $invoiceDetails = json_decode($response, true);

      $i=0;

      if (empty($invoiceDetails['items']['item'])){
        echo "Faktura $invoice_id saknar fakturarader"; exit;
      }

      foreach ($invoiceDetails['items']['item'] as $row) {

        $i++;

        if ($i == 1) {
          // Extract last charachters (date) from item description
          $period = substr($row['description'], -24, -1);
          $period = explode('-', $period);
          // Remap the array so that dates are integers
          $period = array_map('intval',$period);
          // var_dump($period);
          // Validate data so that it really is a date
          if (checkdate($period[1], $period[2], $period[0]) == true) {
            $period_from = substr($row['description'], -24, -14);
          }
          if (checkdate($period[4], $period[5], $period[3]) == true) {
            $period_to = substr($row['description'], -11, -1);
          }
          if (isset($period_from) && isset($period_to)) {
              $invoice_message = "$period_from - $period_to";
          }
        }

        $income_account = substr($row['description'],0,4);
        if (!is_numeric($income_account)) {

          echo "Du har inte valt ett kontonummer för faktura $invoice_id"; exit;
        }

        $item = array(
            'price' => $row['amount'],
            'title' => 'Produkt',
            'description' => $row['description'],
            'count' => 1,
            'vat' => 25,
            'bookkeeping' => array(
             'income_account' => $income_account,
             'vat_account' => "2611",
            ),
        );
        $items[] = $item;
      }

      // Set the due date to 20 days (standard) or 30 days (set in customer profile)
      $longer_due_date = get_client_details_from_whmcs($config, $params);
      $longer_due_date = $longer_due_date['customfields7'];

      if ($longer_due_date == 'on') {
        // Set 30 days due date
        $input_date = date('Y-m-d');
        $duedate = new DateTime($input_date);
        $duedate->modify('+30 days');
        $duedate = $duedate->format('Y-m-d');
      } else {
        // Set 20 days due date
        $input_date = date('Y-m-d');
        $duedate = new DateTime($input_date);
        $duedate->modify('+20 days');
        $duedate = $duedate->format('Y-m-d');
      }

      $billogramObject = $api->billogram->create(array(
        'invoice_date' => $input_date,
        'due_date' => $duedate,
        'currency' => 'SEK',
        'customer' => array(
            'customer_no' => $params["customer_no"],
        ),
        'info' => array(
            'message' => $invoice_message,
        ),
        'creditor_unique_value' => $invoice_id,
        'invoice_fee' => 0,
        'automatic_reminders' => true,
        'items' => $items,
        'callbacks' => array(
            'url' => 'http://wasabiadmin.se/billogram/api/register_transaction.php',
        ),
      ));

      $billogramId = $billogramObject->id;

      /* Send billogram with the delivery method "Letter", could also be "Email"
       or "Email+Letter". The $billogramObject will refresh it's data with
       up to date data. */
      $billogramObject->send('Email+Letter');

      $fields["username"] = $config['whmcs']['username'];
      $fields["password"] = $config['whmcs']['password'];
      $fields["action"] = "updateinvoice";
      $fields["invoiceid"] = $invoice_id;
      // $fields["duedate"] = $invoiceDetails['duedate'];
      $fields["status"] = "Unpaid";
      $fields["notes"] = $billogramId;

      // Call the API
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $config['whmcs']['url'] . 'includes/api.php');
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
      $response = curl_exec($ch);
      if (curl_error($ch)) {
        die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
      }
      curl_close($ch);

      $response = explode(";",$response);
      foreach ($response AS $temp) {
      $temp = explode("=",$temp);
      $results[$temp[0]] = $temp[1];
      }

      $filename = $_SERVER['PHP_SELF'];
      $filename = explode('/', $filename);
      $filename = end($filename);

      if ($filename != 'cron_create_billogram.php') {
          if ($results["result"]=="success") {
            # Result was OK!
            echo 'OK';
            header("Location: http://admin.wasabiweb.se/admin");

          } else {
            # An error occured
            echo "The following error occured: ".$results["message"];
          }
      } else {
        if ($results["result"] != "success") {
          echo "The following error occured: ".$results["message"]; exit;
        } else {
          echo "Invoice $invoice_id created successfully";
        }
      }
    }

}

function register_payment_in_crm ($config, $api, $callback) {

    // Set post values
    $postfields = array(
        'username' => $config['whmcs']['username'],
        'password' => $config['whmcs']['password'],
        'responsetype' => 'json',
        // 'invoiceid' => 35,
        'invoiceid' => $api->billogram->get($callback['billogram']['id'])->info->order_no,
    );

    if ($callback['event']['type'] == "Payment"){
        $postfields['action'] = "addtransaction";
        $postfields['amountin'] = $callback['event']['data']['amount'];
        $postfields['date'] = $callback['date_for_event'];
        $postfields['paymentmethod'] = "Billogram";
    }

    switch ($callback['billogram']['state']){

        case "Ended":
            $postfields['status'] = "Paid";
            $postfields['paymentmethod'] = "Billogram";
            break;

        case "Paid":
            $postfields['status'] = "Paid";
            $postfields['paymentmethod'] = "Billogram";
            break;

        case "Credited":
            $postfields['status'] = "Cancelled";
            $postfields['paymentmethod'] = "Billogram";
            break;

        case "Deleted":
            $postfields['status'] = "Cancelled";
            $postfields['paymentmethod'] = "Billogram";
            break;
    }

    // Call the API
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $config['whmcs']['url'] . 'includes/api.php');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postfields));
    $response = curl_exec($ch);
    if (curl_error($ch)) {
        die('Unable to connect: ' . curl_errno($ch) . ' - ' . curl_error($ch));
    }
    curl_close($ch);

    if ($response=='{"result":"success"}') {
        # Result was OK!
        echo 'OK';

    } else {
        # An error occured
        echo "The following error occured: ".$results["message"];
    }
}

function save_all_billograms_to_json ($api) {

    /* Query billogram api for all billograms */
    $billogramQuery = $api->billogram->query()->
    pageSize(50)->
    order('invoice_date', 'desc');

    /* Billogram only fetch 50 billograms at a time, so check how many pages */
    $totalPages = $billogramQuery->totalPages();

    $billogram_data = array();

    /* Loop through all pages */
    for ($page = 1; $page <= $totalPages; $page++) {

      /* Get all billograms and save the data into an array */
      $billogramArray = $billogramQuery->getPage($page);
      foreach ($billogramArray as $billogram) {

          $billogram_data[] = $billogram->data['id'];

      }
      // Only fetch 500 billograms
      if ($page == 11) {
        // var_dump($billogram_data);
        break;
      }
    }

    /* Encode the billogram data to json and save into billograms.json */
    file_put_contents('billograms.json', '');
    $billogram_data = json_encode($billogram_data);
    $json = fopen('billograms.json', 'w');
    $json_write = fwrite($json, $billogram_data);
    fclose($json);

    /* Return billogram data array */
    if ($json_write) {
        return true;
    } else {
        return false;
    }

}

function save_detailed_billograms_to_json ($api) {

    $invoice_data = array();

    $billogram_data = file_get_contents('billograms.json');
    $billogram_data = json_decode($billogram_data, true);
    // var_dump($billogram_data);

    foreach ($billogram_data as $billogram) {
      $invoice = $api->billogram->get($billogram);
      $invoice_data[] = $invoice->data;
    }

    /* Encode the billogram data to json and save into billograms.json */
    file_put_contents('billograms_details.json', '');
    $billogram_details = json_encode($invoice_data);
    $json = fopen('billograms_details.json', 'w');
    $json_write = fwrite($json, $billogram_details);
    fclose($json);

    /* Return billogram data array */
    if ($json_write) {
        return true;
    } else {
        return false;
    }

}

function get_revenue ($account, $month) {

    $billograms = file_get_contents('billograms_details.json');
    $billograms = json_decode($billograms, true);
    $price = 0;

    /* Loop through all billograms */
    foreach ($billograms as $billogram) {

      /* Date of the invoice */
      $billogram_date = explode('-', $billogram['invoice_date']);

      /* Save billograms into an array, only from this month and year */
      if ($billogram_date[1] == $month && $billogram_date[0] == '2018') {

          /* Check state so the billogram is not cancelled */
          if ($billogram['state'] != 'Credited' && $billogram['state'] != 'Unattested') {
            foreach ($billogram['items'] as $invoice_item) {

              /* Check so that the billogram has the same income account as requested */
              if ($account == $invoice_item['bookkeeping']['income_account']) {

                  /* Save the price into an array */
                  $price = $price + $invoice_item['price'];
              }

            }
          }
      }

    }

    return $price;
}

function get_revenue_details ($account, $month) {

    $billograms = file_get_contents('billograms_details.json');
    $billograms = json_decode($billograms, true);
    $details = [];
    $i = 0;

    /* Loop through all billograms */
    foreach ($billograms as $billogram) {

      /* Date of the invoice */
      $billogram_date = explode('-', $billogram['invoice_date']);

      /* Save billograms into an array, only from this month and year */
      if ($billogram_date[1] == $month && $billogram_date[0] == '2018') {

          /* Check state so the billogram is not cancelled */
          if ($billogram['state'] != 'Credited' && $billogram['state'] != 'Unattested') {
            foreach ($billogram['items'] as $invoice_item) {

              $i++;

              /* Check so that the billogram has the same income account as requested */
              if ($account == $invoice_item['bookkeeping']['income_account']) {

                  // var_dump($billogram);

                  $details[$i]['customer'] = $billogram['customer']['name'];
                  $details[$i]['id'] = $billogram['id'];
                  $details[$i]['amount'] = $invoice_item['price'];

              }

            }
          }
      }

    }

    return $details;
}

function get_credited ($month) {

    $billograms = file_get_contents('billograms_details.json');
    $billograms = json_decode($billograms, true);
    $price = 0;

    /* Loop through all billograms */
    foreach ($billograms as $billogram) {

      if($billogram['detailed_sums']['credited_sum'] < 0) {

        /* Date of last update */
        $billogram_date = explode('-', $billogram['updated_at']);

        /* Save billograms into an array, only from this month and year */
        if ($billogram_date[1] == $month && $billogram_date[0] == '2018') {

          $price_with_vat = $billogram['detailed_sums']['credited_sum'];
          $price_without_vat = $price_with_vat * 0.8;
          $price = $price + $price_without_vat;
        }

      }

    }

    return $price;

}

function get_all_client_invoices($api, $params) {

      $customer_no = check_if_client_exists_in_billogram($api, $params);
      $customer_no = $customer_no->customer_no;


      /* Query billogram api for all billograms */
      $billogramQuery = $api->billogram->query()->
      order('invoice_date', 'desc')->
      pageSize(50)->
      filterField('customer:customer_no', $customer_no);

      $billogram_data = [];

        /* Get all billograms and save the data into an array */
        $billogramArray = $billogramQuery->getPage(1);

        foreach ($billogramArray as $billogram) {

            $billogram_data[] = "$billogram->invoice_no, $billogram->invoice_date, $billogram->due_date, $billogram->total_sum kr, $billogram->state, $billogram->ocr_number";

        }

      return $billogram_data;
}

function get_pdf_invoice($api, $invoice_no) {

  /* Query billogram api for all billograms */
  $billogramQuery = $api->billogram->query()->
  order('invoice_date', 'desc')->
  pageSize(1)->
  filterField('ocr_number', $invoice_no);

  $billogramArray = $billogramQuery->getPage(1);

  foreach ($billogramArray as $billogram) {

      $invoice = $billogram->getInvoicePdf();
      $invoice = base64_encode($invoice);
  }

  return $invoice;

}
