<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

use Billogram\Api as BillogramAPI;
use Billogram\Api\Exceptions\ObjectNotFoundError;

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) .
            DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
    require $fileName;
}
spl_autoload_register('autoload');

// Include Config
$config = include 'config.php';

// Load Billogram API in $api
$api = new BillogramAPI(
  $config['billogram']['api_id'],
  $config['billogram']['api_password'],
  $config['billogram']['identifier'],
  $config['billogram']['url']
);

// Include Function
include('functions.php');


/*----------------------------------------------------------------------------*/

?>

<style>
table {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

table td, table th {
    border: 1px solid #ddd;
    padding: 8px;
}

table tr:nth-child(even){background-color: #f2f2f2;}

table tr:hover {background-color: #ddd;}

table th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #57bbb1;
    color: white;
}

.total {
  font-weight: bold;
}
</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	function tally (selector) {
		$(selector).each(function () {
			var total = 0,
				column = $(this).siblings(selector).andSelf().index(this);
			$(this).parents().prevUntil(':has(' + selector + ')').each(function () {
				total += parseFloat($('td.sum:eq(' + column + ')', this).html()) || 0;
			})
			$(this).html(total);
		});
	}
	// tally('td.subtotal');
	tally('td.total');
});

</script>

<table>
  <tr>
    <th>Typ</th>
    <th>Januari</th>
    <th>Februari</th>
    <th>Mars</th>
    <th>April</th>
    <th>Maj</th>
    <th>Juni</th>
    <th>Juli</th>
    <th>Augusti</th>
    <th>September</th>
    <th>Oktober</th>
    <th>November</th>
    <th>December</th>
  </tr>
  <tr>
    <td>3010 - Webbproduktion (CM), engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3010&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3010", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3011 - Webbproduktion (WL), engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3011&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3011", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3012 - Innehållshantering</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3012&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3012", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3013 - Design, engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3013&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3013", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3014 - SEO, engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3014&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3014", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3015 - SEO, löpande/mån</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3015&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3015", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3016 - SEO-tvätt, engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3016&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3016", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3017 - Copy, engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3017&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3017", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3018 - Content marketing, löpande/mån</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3018&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3018", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3019 - Sociala medier, engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3019&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3019", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  <tr>
    <td>3020 - Sociala medier (produktion/hantering), löpande/mån</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3020&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3020", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3021 - Video, engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3021&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3021", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3022 - Video, löpande/mån</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3022&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3022", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3023 - Nyhetsbrev, engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3023&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3023", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3024 - Adwordsarvode</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3024&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3024", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3025 - Övrigt marknad (analytics osv)</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3025&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3025", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3026 - Annonsering/Övrig konsulting</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3026&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3026", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3027 - WW academy</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3027&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3027", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3028 - Workshop</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3028&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3028", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3029 - Kundtjänst, engångsintäkt</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3029&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3029", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3030 - Hosting, löpande/mån</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3030&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3030", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3031 - Serviceavtal, löpande/mån</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3031&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3031", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3032 - CMS Sweden, löpande/mån</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3032&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3032", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3033 - Google Apps, löpande/mån</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3033&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3033", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3034 - Domäner, löpande/mån</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3034&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3034", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>3090 - #GeTillbaka</td>
    <?php for ($i = 1 ; $i < 12; $i++): ?>
      <td class="sum">
        <a href="get_revenue_details.php?account=3090&month=<?php echo $i; ?>">
          <?php echo round(get_revenue("3090", $i)); ?>
        </a>
      </td>
    <?php endfor; ?>
  </tr>
  <tr>
    <td>Krediteringar</td>
    <td><?php echo round(get_credited("01")); ?></td>
    <td><?php echo round(get_credited("02")); ?></td>
    <td><?php echo round(get_credited("03")); ?></td>
    <td><?php echo round(get_credited("04")); ?></td>
    <td><?php echo round(get_credited("05")); ?></td>
    <td><?php echo round(get_credited("06")); ?></td>
    <td><?php echo round(get_credited("07")); ?></td>
    <td><?php echo round(get_credited("08")); ?></td>
    <td><?php echo round(get_credited("09")); ?></td>
    <td><?php echo round(get_credited("10")); ?></td>
    <td><?php echo round(get_credited("11")); ?></td>
    <td><?php echo round(get_credited("12")); ?></td>
  </tr>
  <tr>
    <td></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
    <td class="total"></td>
  </tr>

</table>
